<?php
include "functions.php";//segéd eljárások betöltése
//ha van $_POST adat akkor 'hibakezelés'
//mappa ellenőrzése, létrehozása
$dir = 'data/';//ebbe a mappába szeretnénk dolgozni
if(!is_dir($dir)){//ha nem létezik a mappa
    mkdir($dir, 0755, true);
}
if (!empty($_POST)) { //operátor -> ! - negálás 'nem'
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $errors = [];//ide gyűjtjük a hibákat
    //Név mező nem lehet üres
    $name = strip_tags(filter_input(INPUT_POST, 'name'));//szűrés és spacek eltávolítása
    $name = trim($name);//ltrim rtrim - szövegvégi spacek eltávolítása
    if (mb_strlen($name, 'utf-8') < 3) {
        $errors['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }

    //email legyen email formátum
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

    if (!$email) {
        $errors['email'] = '<span class="error">Érvénytelen formátum!</span>';
    }

    //jelszó 1 és 2
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    //jelszó 1 legyen min 6 karakter
    if (mb_strlen($password) < 6) {//nincs 6 karakter a password mezőben
        $errors['password'] = '<span class="error">Legalább 6 karakter!</span>';
    } elseif ($password !== $repassword) {//nem egyeztek a beírt jelszavak
        $errors['repassword'] = '<span class="error">A jelszavak nem egyeztek</span>';
    } else {//jelszó rendben formailag
        //jelszó elkódolása, mert soha nem tárolunk jelszót visszaolvasható formában
        //echo $password = md5($password);elavult/nem biztonságos
        $password = password_hash($password, PASSWORD_BCRYPT);
    }

    if (empty($errors)) {
        //üres maradt a hibatömb hibakezelés után
        //adatok 'tisztázása'
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password
        ];
        //mikor történt az űrlap elküldése
        $data['time_created'] = date('Y-m-d H:i:s');
        echo '<pre>' . var_export($data, true) . '</pre>';
        //file-ba írás
        $fileName = 'users.json';
        //van e már benne adat?
        if(file_exists($dir.$fileName)){
            $usersFromJson = file_get_contents($dir.$fileName);
            $users = json_decode($usersFromJson,true);//eddigi userek
        }else{//nem voltak még userek azaz nincs adatfile
            $users = [];
        }
        //mostani adatok beillesztése a tömbbe
        $users[] = $data;
        $jsonData = json_encode($users);//átalakítás stringgé
        file_put_contents($dir.$fileName,$jsonData);//file ba írás
        echo '<pre>' . var_export($users, true) . '</pre>';
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlap feldolgozása - azonos fileban</title>
    <style>
        form, label {
            display: flex;
            flex-flow: column nowrap;
        }

        form {
            max-width: 350px;
            margin: 0 auto;
        }

        label {
            margin: 5px 0;
        }

        .error {
            color: #f00;
            font-style: italic;
            font-size: 0.8em;
        }
    </style>
</head>
<body>
<section>
    <h1>Töltse ki az űrlapot</h1>
    <!--@todo HF űrlap átalakítása 1 php stringgé $form-->

        <!--Név-->
        <?php
        $form = '<form method="post">';
        $form .= '<label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" placeholder="Gipsz Jakab" value="'.getValue('name').'">';
        $form .= getError('name');//hiba hozzáfűzése
        $form .= '</label>';
        //email
        //password
        //repassword


        //kiírás 1 lépésben
        echo $form;
      ?>

        <!--Email-->
        <label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" placeholder="email@cim.hu" value="<?php echo getValue('email'); ?>">
            <?php
            //ha van hibája az elemnek, akkor kiírjuk
            echo getError('email');
            ?>
        </label>
        <!--Jelszó 1-->
        <label>
            <span>Jelszó<sup>*</sup></span>
            <input type="password" name="password" placeholder="******" value="">
            <?php
            //ha van hibája az elemnek, akkor kiírjuk
            echo getError('password');
            ?>
        </label>
        <!--Jelszó újra-->
        <label>
            <span>Jelszó újra<sup>*</sup></span>
            <input type="password" name="repassword" placeholder="******" value="">
            <?php
            //ha van hibája az elemnek, akkor kiírjuk
            echo getError('repassword');
            ?>
        </label>
        <button>Mehet</button>
    </form>
</section>
</body>
</html>

