<?php
//file betöltése, mintha ide lenne gépelve
include "functions.php";//ha nincs meg a forrás, warning és tovább
//require "functions.php";//ha nincs meg a forrás, fatal_error
include_once "functions.php";//ha még nem volt betöltve, betölti
require_once "functions.php";//ha még nem volt betöltve, betölti

//17.Kérjünk be egy N természetes számot, majd írassuk ki a három összes olyan többszörösét, amely kisebb vagy egyenlő mint N.
if(!empty($_POST)){
    $errors = [];
    $n = filter_input(INPUT_POST, 'N',FILTER_VALIDATE_INT);
    var_dump($n);
    if($n === false || $n === null){
        $errors['N'] = '<span class="error">Ez nem egész szám!</span>';
    }

    if(empty($errors)){
        //írassuk ki a három összes olyan többszörösét, amely kisebb vagy egyenlő mint N.
        for($i = 3;$i <= $n; $i += 3){
            echo "<br>$i";
        }
        //ugyanaz while-al
        /*$i = 3;
        while($i <= $n){
            echo "<br>$i";
            $i += 3;
        }*/

    }

}


$form = '<form method="post">';//form nyitása
//input mező
$form .= '<label>
            <span>Írj ide egy természetes számot<sup>*</sup></span>';//label nyitás, és mező felirat
$form .= '<input type="text" name="N" value="'.getValue('N').'" placeholder="1234">';//input mező
$form .= getError('N');//hiba űrlapba 'fűzése'
$form .= '</label>';//label zárása
$form .= '<button>mehet</button>';//küldés gomb
$form .= '</form>';//form zárása
//kiírás
echo $form;

//ideiglenes stílusok
$styles = '<style>
        form, label  {
            display:flex;
            flex-flow: column nowrap;
        }
        form {
            max-width: 350px;
            margin:0 auto;
        }
        label {
            margin: 5px 0;
        }
        .error {
            color:#f00;
            font-style:italic;
            font-size:0.8em;
        }
    </style>';

echo $styles;
