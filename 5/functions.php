<?php
/**
 * value visszaadása input mezőkbe (paraméter átadással)
 * @param $fieldName
 * @return mixed
 */
function getValue($fieldName){
    return filter_input(INPUT_POST, $fieldName);
}

/**
 * hiba kiírás segédfüggvénye
 * @param $fieldName
 * @return string
 */
function getError($fieldName){
    global $errors;//az eljárás idejére globális, azaz 'látni' fogjuk az eljáráson belül

    if(isset($errors[$fieldName])){
        return $errors[$fieldName];//visszatérünk a hibaüzenettel
    }
    return '';//nincs ilyen elem visszatérünk üres stringgel
}
