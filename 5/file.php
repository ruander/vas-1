<?php
//mappa ellenőrzése, létrehozása
$dir = 'data/';//ebbe a mappába szeretnénk dolgozni
if(!is_dir($dir)){//ha nem létezik a mappa
    mkdir($dir, 0755, true);
}

//file létrehozása
//@todo HF átnézni, php.net: fopen(),fread,fwrite,fclose,

$content = 'Ez egy újabb szöveg.....';//ez lesz a file tartalma
$fileName = 'test.txt';//ez lesz a file neve
//művelet (fileba írás)
var_dump(file_put_contents($dir.$fileName,$content/*,FILE_APPEND*/));//a file put contents az éppen kiírt adat mérete byte-ban


//olvassuk be a file tartalmát
$contentFromFile = file_get_contents($dir.$fileName);
var_dump($contentFromFile);
//egészítsük ki a file tartalmát
$newContent = $contentFromFile.'<b>Horváth György</b>';
file_put_contents($dir.$fileName,$newContent);

//a file get contents urlt fogad paraméterként: pl
$test = file_get_contents('https://ruander.hu');
echo $test;
