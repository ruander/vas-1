<?php
//file betöltése, mintha ide lenne gépelve
require "functions.php";


//18.Kérjünk be két természetes számot (M,N), majd rajzoljunk ki a 'böngészőbe' egy MxN méretű téglalapot csillag (*) jelekből.
if(!empty($_POST)){
    $errors = [];
    $n = filter_input(INPUT_POST, 'N',FILTER_VALIDATE_INT);
    var_dump($n);
    if($n <= 0){
        $errors['N'] = '<span class="error">Ez nem pozitív egész szám!</span>';
    }
    $m = filter_input(INPUT_POST, 'M',FILTER_VALIDATE_INT);
    var_dump($m);
    if($m <= 0){
        $errors['M'] = '<span class="error">Ez nem pozitív egész szám!</span>';
    }

    if(empty($errors)){
        for($i=1;$i<=$n;$i++){
            echo '<br>';//új sor
            //belső ciklus a csillagoknak
            for($j = 1;$j<=$m;$j++){
                echo '*';
            }

        }
    }

}


$form = '<form method="post">';//form nyitása
//input mező
$form .= '<label>
            <span>Írj ide pozitív egész számot (csillagok száma)<sup>*</sup></span>';//label nyitás, és mező felirat (M)
$form .= '<input type="text" name="M" value="'.getValue('M').'" placeholder="1234">';//input mező
$form .= getError('M');//hiba űrlapba 'fűzése'
$form .= '</label>';//label zárása (M)

///N mező
$form .= '<label>
            <span>Írj ide pozitív egész számot (sor)<sup>*</sup></span>';//label nyitás, és mező felirat
$form .= '<input type="text" name="N" value="'.getValue('N').'" placeholder="1234">';//input mező
$form .= getError('N');//hiba űrlapba 'fűzése'
$form .= '</label>';//label zárása
$form .= '<button>mehet</button>';//küldés gomb
$form .= '</form>';//form zárása
//kiírás
echo $form;

//ideiglenes stílusok
$styles = '<style>
        form, label  {
            display:flex;
            flex-flow: column nowrap;
        }
        form {
            max-width: 350px;
            margin:0 auto;
        }
        label {
            margin: 5px 0;
        }
        .error {
            color:#f00;
            font-style:italic;
            font-size:0.8em;
        }
    </style>';

echo $styles;
