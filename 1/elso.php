<!doctype html>
<html lang="hu">
<head>
 <meta charset="utf-8">
 <title>Első PHP fileom</title>
</head>
<body>
Hello World!<br>
<?php
/*
blokk
komment
*/
//line komment
echo 'Hello World!';//kiírás a standard output-ra | '',"" -> operátor: string | ;-> utasítás lezárása
print "<br>Hellow Lord!";//kiírás a standard output-ra

//változók kezelése - primitívek
$number = 143;//operátor: $-> változó; = -> értékadó operátor | tipus: integer (int)
$name = "Horváth György"; // tipus: string
$price = 99.5; //tipus: floating point number (float)
$is_michael_jackson_alive = false;//tipus: logikai - boolean (bool) 
//$snake_case_variable_name ; camelCaseVariableName

$sum = $number + $price*10;
echo '<pre>';
//változó információk kiírása (fejlesztés alatt!)
var_dump($number,$name,$price,$is_michael_jackson_alive, $sum, $non_exist);
echo '</pre>';

//generáljunk egy egész számot 1-100 közé
$randomNumber = rand(1,100);

echo '<div>A generált szám: ' . $randomNumber . '</div>';//operátor: . -> konkatenáció, a 2 oldalán található stringet eggyé fűzi 

//nem primitív értékekkel
$array = [];//tipus: array | üres tömb létrehozása v. ürítése

echo '<pre>'.var_export($array, true).'</pre>';

$array = [124,"szöveg valami", 23/17, true];//tömb megadása értékekkel
echo '<pre>'.var_export($array, true).'</pre>';

//tömb elemeinek bővítése automatikus indexen
$array[] = 'új elem 1';

//2.
array_push($array, 'új elem 2.');


$array[100] = '100-as index';
//tömb elemszáma:
var_dump(count($array));
echo '<pre>'.var_export($array, true).'</pre>';

//asszociatív index
$array['username'] = 'Horváth György';
echo '<pre>'.var_export($array, true).'</pre>';
//tömb elemszáma:
var_dump(count($array));

//asszociativ tömb minta:
$user = [
	'id' => 1,
	'name' => 'George Horváth',
	'email'=> 'hgy@iworkhsop.hu',
	'is_active' => true
];
echo '<pre>'.var_export($user, true).'</pre>';
echo '<h3>Hello kedves '.$user['name'].'!</h3>';
?>
</body>
</html>