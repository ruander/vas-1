<?php
//PURE PHP file (csak PHP kód, nem lesz PHP záró tag)
//Állandók
const THIS_IS_A_CONSTANT = 'állandó érték';
echo THIS_IS_A_CONSTANT;
//const PI = pi(); konstans nem tartalmazhat eljárást az értéke megadásakor
//THIS_IS_A_CONSTANT = 'másik érték'; konstans nem vehet fel újabb értéket soha

/****************CIKLUSOK*******************/
//FOR ciklus (elöltesztelő)
/*
for(ciklusváltozó kezdeti értéke;belépési feltétel(ciklusváltozó) vizsgálata,ciklusváltozó léptetése){
    ciklusmag...
}
 */
//írjuk ki a számokat 1-10ig
for($i=1;$i<=10;$i++){//operátor: ++, növelés 1el
    //kiírás
    echo "<br>$i";
}
/*for($i='A';$i<='Z';$i++){//operátor: ++, növelés '1'el
    //kiírás
    echo "<br>$i";
}*/
//WHILE ciklus (elöltesztelő)
/*
while(belépési feltétel vizsgálata){
    ciklusmag...
}
 */
$i=1;
while($i<=10){
    echo "<br>".$i++;
}

$randomNumber = rand(1,10);
while($randomNumber<=5){
    echo "<br>$randomNumber";
    $randomNumber = rand(1,10);
}
echo '<hr>';
/*
//forral egyanez
$randomNumber = rand(1,10);
for(;$randomNumber<=5;){
    echo "<br>$randomNumber";
    $randomNumber = rand(1,10);
}*/
//WHILE (hátultesztelő) - mindenképpen 1x lefut
$randomNumber = rand(1,10);
do{
    echo "<br>$randomNumber";
    $randomNumber = rand(1,10);
}while($randomNumber<=5);

//FOREACH
$users = [ 'Tercsi','Fercsi','Kata','Klára' ];
echo '<pre>'.var_export($users,true).'</pre>';
//oldschool js-like FOR
for($i=0;$i<count($users);$i++){
    //névlista
    echo "<br>".$users[$i];
}
//asszociatív tömb
$user = [
    'id' => 1,
    'name' => 'George Horváth',
    'email'=> 'hgy@iworkhsop.hu'
];
echo '<pre>'.var_export($user,true).'</pre>';
//Tömb (objektum) bejárása FOREACH
/*
foreach($tomb as $key => $value){
    ciklusmag ($key és $value elérhető)
}
 */
foreach($user as $k => $v){
    echo "<br>Aktuális index (kulcs): $k | érték: $v";
}
