<?php
//IF
/*
if(feltétel){
    feltétel == igaz VAGY feltétel != hamis
    //kód... (igaz (true) ág)
}else{
    feltétel != igaz VAGY feltétel == hamis
    //másik kód...(hamis (false) ág)
}
 */
//'Dobjunk' 6 oldalú kockával
$randomNumber = rand(0,2);
if($randomNumber%2 == 0){
    //igaz ág
    echo "<br>$randomNumber, ami páros.";
}else{
    //hamis ág
    echo "<br>$randomNumber, ami páratlan.";
}
//fordított feltétellel
if($randomNumber%2 != 0){
    //igaz ág
    echo "<br>$randomNumber, ami páratlan.";
}else{
    //hamis ág
    echo "<br>$randomNumber, ami páros.";
}


//SWITCH
/*
switch(változó){
    case 'a':
        kód ha a változó értéke 'a'
    break;

    case 'b':
        kód ha a változó értéke 'b'
    break;

    case 'c':
        kód ha a változó értéke 'c'
    break;

    default:
        kód ha a felsorolt értékek közül egyik sem
    break;
}
 */
$winningClass = rand(0,4);//nyereményosztály
echo "<h2>Nyereményosztály: $winningClass</h2>";

switch($winningClass){
    case 1:
        echo 'Visszajött a szelvény ára...';
        break;
    case 2:
        echo 'Visszajött egy havi játék ára...';
        break;
    case 3:
        echo 'Vehetsz új HiFi-t, TV-t, almástelót...';
        break;
    case 4:
        echo 'Bye bye meló....';
        break;
    default:
        echo "Sajnos ment a levesbe...";
        break;
}
