<?php
//1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat rak ki.
/*
 * @todo HF: php gyakorló txt összes
 * @todo HF 2: feladatgyüjtemény.pdf 1-5
 */
//a)
for ($i = 1; $i <= 4; $i++) {
    echo 'Menü';
}

//b)
for ($i = 1; $i <= 4; $i++) {
    echo '<a href="#">Menü</a>';
}

//c)
echo '<ul>';
for ($i = 1; $i <= 4; $i++) {
    echo '<li><a href="#">Menü</a></li>';
}
echo '</ul>';

//....

//z)
$menu = [
    1 => 'Home',
    2 => 'About',
    3 => 'Services',
    4 => 'Contact',
    5 => 'Extra'
];
$menuRender = '<nav><ul>';//menü html elemek kiírása (nyitás)

//bejárom a menütömböt
foreach ($menu as $menuID => $menuTitle) {

    $menuRender .= '<li><a href="#menu-'.$menuID.'">'.$menuTitle.'</a></li>';//operátor .= hozzáfűzi a bal oldalon található értékhez a jobb oldalon található értéket (konkatenáció) // $a = $a.'valami' -> $a .='valami' |(+=,-=,*=,/=)
}

$menuRender .= '</ul></nav>';//menü html elemek kiírása (zárás)

echo $menuRender;

//4. Készítsünk programot, amely kiszámolja az első 100 darab. páros szám összegét (A ciklus vegyük egytől ötvenig, majd a ciklusmagban vegyük a ciklusváltozó kétszeresét - így megkapjuk a páros számokat. Ezeket hasonlóan adjuk össze, mint az első feladatban).
$sum = 0;
for($i=1;$i<=50;$i++){
    $sum += $i*2;
}
$exercise_4 = '<div>A páros számok összege 1 -100 ig:'.$sum.'</div>';
echo $exercise_4;
