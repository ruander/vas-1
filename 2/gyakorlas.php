<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gyakorló feladatok</title>
</head>
<body>
<?php
/**
 * PHP MATH eljárások: https://www.php.net/manual/en/ref.math.php
 * */
//1. Írjon egy php programot, amely kiszámolja és kiírja a 2 m élhosszúságú kocka felületét.
$a = 2;
$area = pow($a,2)*6;//$a*$a*6;//felület
echo "<div>Egy {$a}m élhosszúságú kocka felülete: {$area}m<sup>2</sup></div>";
echo '<div>Egy '.$a.'m élhosszúságú kocka felülete: '.$area.'m<sup>2</sup></div>';
echo "<div>Egy \\mappa\\$a m \"élhosszúságú\" kocka felülete: \$area m<sup>2</sup></div>";
echo '<div>Egy $a m "élhosszúságú" kocka felülete: $area m<sup>2</sup> - Peter O\'Toole</div>';

//16.Írjon egy programot, amely kiszámolja az 1*3*5*7*9*11*13*15*17*19 értéket ciklus utasítás segítségével.
$mtpl = 1;

for($i=1;$i<=19;$i+=2){ //operátor $i = $i + 2 -> $i+=2
    $mtpl *= $i;
}
echo "<br>1*3*5*7*9*11*13*15*17*19 = $mtpl";

//19.Írjon egy programot ciklus utasítást használva, amely az alábbi elrendezésű szöveget írja ki a képernyőre:
//2
//22
//222
//2222
$char = '2';
for($i=1;$i<=4;$i++){
    echo '<br>';
    for($j=1;$j<=$i;$j++){
        echo $char;
    }
}
for($i=1;$i<=4;$i++){
        echo '<br>'.str_repeat($char,$i);
}
?>
</body>
</html>
