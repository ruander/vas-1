<?php

require "database.php";/** @var $link mysqli db csatlakozás betöltése*/


$qry = "SELECT 
            customername,
            c.country country,
            c.city city
        FROM offices off
        LEFT JOIN employees e
            ON e.officecode = off.officecode
        LEFT JOIN customers c
            ON e.employeenumber = salesrepemployeenumber
        WHERE off.country = 'USA' ";

//lekérés futtatása
$result = mysqli_query($link,$qry) or die(mysqli_error($link));
//kibontás/bejárás - kiírás -> nem jó mert doctype előtt vagyunk ezért bodyban kell megtenni
//kibontás/bejárás - table->változóba gyüjtése while ciklussal
$table = '<table>
            <tr>
             <th>Ország</th>
             <th>Város</th>
             <th>Név</th>
            </tr>';
while( $row = mysqli_fetch_assoc($result) ) {
    //var_dump($row);
    $table .= '<tr>
                 <td>'.$row['country'].'</td>
                 <td>'.$row['city'].'</td>
                 <td>'.$row['customername'].'</td>
                </tr>';
}
$table .= '</table>';

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Adatbázis lekérések 15. feladat megvalósítása</title>
</head>
<body>
<h1>15.	listázzuk ki az amerikai (az iroda van ott) megrendelőket: név, város, ország</h1>
<?php
//kiírás
echo $table;
?>
</body>
</html>
