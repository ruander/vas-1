<?php
/**
 * value visszaadása input mezőkbe (paraméter átadással)
 * @param $fieldName
 * @param $rowData
 * @return mixed
 */
function getValue($fieldName, $rowData = []){
    if(filter_input(INPUT_POST, $fieldName) !== null){//ha létezik az elem akkor visszatér vele (akkor is ha üres)
        return filter_input(INPUT_POST, $fieldName);
    }
    //ha van DB adat akkor azzal térünk vissza
    if(array_key_exists($fieldName,$rowData)){
        return $rowData[$fieldName];
    }

    return '';
}

/**
 * hiba kiírás segédfüggvénye
 * @param $fieldName
 * @return string
 */
function getError($fieldName){
    global $errors;//az eljárás idejére globális, azaz 'látni' fogjuk az eljáráson belül

    if(isset($errors[$fieldName])){
        return $errors[$fieldName];//visszatérünk a hibaüzenettel
    }
    return '';//nincs ilyen elem visszatérünk üres stringgel
}
