<?php
require "database_tanfolyam.php";/**  adatbázis csatlakozás @var $link mysqli */
include "functions.php";//segéd eljárások betöltése

if (!empty($_POST)) { //operátor -> ! - negálás 'nem'
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $errors = [];//ide gyűjtjük a hibákat
    //Név mező nem lehet üres
    $name = trim(strip_tags(filter_input(INPUT_POST, 'name')));//szűrés és végződő spacek,tagek eltávolítása

    if (mb_strlen($name, 'utf-8') < 3) {
        $errors['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }

    //email legyen email formátum
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $errors['email'] = '<span class="error">Érvénytelen formátum!</span>';
    }else{
        //email foglaltság ellenőrzése
        $qry = "SELECT id FROM admins WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link,$qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //var_dump($row);
        if($row !== null){
            $errors['email'] = '<span class="error">Már foglalt email cím!</span>
<script>let a = document.querySelector("[name=email]").value</script>';
        }
    }

    //jelszó 1 és 2
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    //jelszó 1 legyen min 6 karakter
    if (mb_strlen($password) < 6) {//nincs 6 karakter a password mezőben
        $errors['password'] = '<span class="error">Legalább 6 karakter!</span>';
    } elseif ($password !== $repassword) {//nem egyeztek a beírt jelszavak
        $errors['repassword'] = '<span class="error">A jelszavak nem egyeztek</span>';
    } else {//jelszó rendben formailag
        //jelszó elkódolása, mert soha nem tárolunk jelszót visszaolvasható formában
        //echo $password = md5($password);elavult/nem biztonságos
        $password = password_hash($password, PASSWORD_BCRYPT);
    }

    //státusz mező
    $status = filter_input(INPUT_POST, 'status')?:0;

    if (empty($errors)) {
        //üres maradt a hibatömb hibakezelés után
        //adatok 'tisztázása'
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'status' => $status
        ];
        //mikor történt az űrlap elküldése
        $data['time_created'] = date('Y-m-d H:i:s');
        echo '<pre>' . var_export($data, true) . '</pre>';
        //adatbázisba mentés
        $qry = "INSERT INTO 
                admins(
                       `name`,
                       `email`,
                       `password`,
                       `status`,
                       `time_created`
                       ) 
                VALUES(
                       '{$data['name']}',
                       '{$data['email']}',
                       '{$data['password']}',
                       '{$data['status']}',
                       '{$data['time_created']}'
                       )";
        //query futtatása
        mysqli_query($link,$qry) or die(mysqli_error($link));
        //átirányítás a listára
        header('location:admins.php');
        exit();
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlap feldolgozása - azonos fileban</title>
    <style>
        form, label {
            display: flex;
            flex-flow: column nowrap;
        }

        form {
            max-width: 350px;
            margin: 0 auto;
        }

        label {
            margin: 5px 0;
        }

        .error {
            color: #f00;
            font-style: italic;
            font-size: 0.8em;
        }
    </style>
</head>
<body>
<section>
    <h1>Töltse ki az űrlapot</h1>
    <?php
    //űrlap
    $form = '<form method="post">';//form elemek változója
    //név
    $form .= '<label>
        <span>Név<sup>*</sup></span>
        <input
            type="text"
            name="name"
            placeholder="Gipsz Jakab"
            value="' . getValue('name') . '">';

    $form .= getError('name');//mezőhiba kiírása
    $form .= '</label>';

    //email ...
    $form .= '<label>
    <span>Email<sup>*</sup></span>
    <input 
            type="text"
            name="email"
            placeholder="your@email.com"
            value="' . getValue('email') . '"
    >';

    $form .= getError('email');

    $form .= '</label>
<!--Jelszó 1-->
    <label>
        <span>Jelszó<sup>*</sup></span>
        <input
                type="password"
                name="password"
                placeholder="******"
                valeu=""
        >';

    $form .= getError('password');

    $form .= '</label>
<!--Jelszó újra-->
    <label>
        <span>Jelszó úrja<sup>*</sup></span>
        <input
                type="password"
                name="repassword"
                placeholder="******"
                value=""
        >';

    $form .= getError('repassword');

    $form .= '</label>';
    //státusz mező maradjon kipipálva ha ki volt
    $checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';

    $form .= '<label>
        <span>
        <input type="checkbox" name="status" value="1" ' . $checked . '> Státusz
        </span>';
    $form .= '</label>
<button>Küldés</button>
</form>';

    //kiírás 1 lépésben
    echo $form;
    ?>
</section>
</body>
</html>

