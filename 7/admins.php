<?php
//Create,Read,Update,Delete - CRUD
require "database_tanfolyam.php";
/** @var $link mysqli */
require "functions.php";//saját eljárások betöltése
//erőforrások
$action = filter_input(INPUT_GET, 'action');//mit akarunk csinálni
$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);//melyik elemmel


//var_dump($action,$id);
if (!empty($_POST)) { //operátor -> ! - negálás 'nem'
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $errors = [];//ide gyűjtjük a hibákat
    //Név mező nem lehet üres
    $name = trim(strip_tags(filter_input(INPUT_POST, 'name')));//szűrés és végződő spacek,tagek eltávolítása

    if (mb_strlen($name, 'utf-8') < 3) {
        $errors['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }

    //email legyen email formátum
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $errors['email'] = '<span class="error">Érvénytelen formátum!</span>';
    } else {
        //email foglaltság ellenőrzése
        $qry = "SELECT id FROM admins WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //var_dump($row, $id);

        if ($row !== null && $row[0] != $id) {//edit miatt kiegészítés, ha módosítunk, lesz id-nk, és az alapján tudjuk tesztelni, hogy a saját emailje-e
            $errors['email'] = '<span class="error">Már foglalt email cím!</span>';
        }
    }

    $updatePassword = false;//segédváltozó hogy kell e majd password a db query
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    if ($action == 'create' || $password != '') {//jelszó 1 és 2 ellenőrzése create esetben és ha az 1. mezőben legalább 1 karakter van
        //jelszó 1 legyen min 6 karakter
        if (mb_strlen($password) < 6) {//nincs 6 karakter a password mezőben
            $errors['password'] = '<span class="error">Legalább 6 karakter!</span>';
        } elseif ($password !== $repassword) {//nem egyeztek a beírt jelszavak
            $errors['repassword'] = '<span class="error">A jelszavak nem egyeztek</span>';
        } else {//jelszó rendben formailag
            //jelszó elkódolása, mert soha nem tárolunk jelszót visszaolvasható formában
            //echo $password = md5($password);elavult/nem biztonságos
            $password = password_hash($password, PASSWORD_BCRYPT);
            $updatePassword = true;
        }
    }
    //státusz mező
    $status = filter_input(INPUT_POST, 'status') ?: 0;

    if (empty($errors)) {
        //üres maradt a hibatömb hibakezelés után
        //adatok 'tisztázása'
        $data = [
            'name' => $name,
            'email' => $email,
            'status' => $status
        ];
        //jelszó?
        if ($updatePassword) {
            $data['password'] = $password;
        }

        if ($action == 'create') {//új felvitel esetén
            //mikor történt az űrlap elküldése
            $data['time_created'] = date('Y-m-d H:i:s');
            echo '<pre>' . var_export($data, true) . '</pre>';
            //adatbázisba mentés
            $qry = "INSERT INTO 
                admins(
                       `name`,
                       `email`,
                       `password`,
                       `status`,
                       `time_created`
                       ) 
                VALUES(
                       '{$data['name']}',
                       '{$data['email']}',
                       '{$data['password']}',
                       '{$data['status']}',
                       '{$data['time_created']}'
                       )";
        } else {//módosítás
            //mikor történt az űrlap elküldése
            $data['time_updated'] = date('Y-m-d H:i:s');
            //ha van jelszó adat, qry kiegészítés
            $passwordQuery = $updatePassword ? ",password = '{$data['password']}'" : '';
            //update query
            $qry = "UPDATE admins 
                SET
                    name = '{$data['name']}',
                    email = '{$data['email']}',
                    status = '{$data['status']}',
                    time_updated = '{$data['time_updated']}'
                    $passwordQuery
                WHERE id = '$id'
                LIMIT 1";
        }

        //query futtatása
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //átirányítás a listára
        header('location:admins.php');
        exit();
    }
}


switch ($action) {
    case 'delete':
        //törlés ha van id-nk
        if ($id) {
            mysqli_query($link, "DELETE FROM admins WHERE id = '$id' LIMIT 1") or die(mysqli_error($link));
        }
        //vissza a listára (url paraméterek nélkül)
        header('location:' . $_SERVER['PHP_SELF']);
        exit();
        break;

    case 'edit':
        $title = "Módosítás [--$id--]";
        if ($id) {
            //adatok lekérése id alapján
            $qry = "SELECT name,email,status FROM admins WHERE id = '$id' LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);//!associativ tömb legyen!
            echo '<pre>' . var_export($row, true) . '</pre>';
        }
        //break;

    case 'create':
        //cím
        $title = $title ?? 'Új felvitel';
        $row = $row ?? []; //isset($row) ? $row : []; ha létezik a row (edit ág) akkor legyen önmaga, különben üres tömb
        //űrlap az adatokkal
        $form = '<h2>'.$title.'</h2>
                <a href="'.$_SERVER['PHP_SELF'].'" >vissza a listára</a>
                <form method="post">';//form elemek változója
        //név
        $form .= '<label>
        <span>Név<sup>*</sup></span>
        <input
            type="text"
            name="name"
            placeholder="Gipsz Jakab"
            value="' . getValue('name', $row) . '">';

        $form .= getError('name');//mezőhiba kiírása
        $form .= '</label>';

        //email ...
        $form .= '<label>
    <span>Email<sup>*</sup></span>
    <input 
            type="text"
            name="email"
            placeholder="your@email.com"
            value="' . getValue('email', $row) . '"
    >';

        $form .= getError('email');

        $form .= '</label>
<!--Jelszó 1-->
    <label>
        <span>Jelszó<sup>*</sup></span>
        <input
                type="password"
                name="password"
                placeholder="******"
                value=""
        >';

        $form .= getError('password');

        $form .= '</label>
<!--Jelszó újra-->
    <label>
        <span>Jelszó úrja<sup>*</sup></span>
        <input
                type="password"
                name="repassword"
                placeholder="******"
                value=""
        >';

        $form .= getError('repassword');

        $form .= '</label>';
        //státusz mező maradjon kipipálva ha ki volt
        //$checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';

        $checked = '';
        //ki kell pipálni ha,
        //nincs post egyáltalán (első betöltés), és adatbázis status 1
        //vagy ha a postból jön a status
        if (empty($_POST) && getValue('status', $row) || getValue('status')) {
            $checked = 'checked';
        }

        /*$checked = 'checked';
        //nem kell kipipálni, ha
        //a) ha a row status 0 és post sincs  empty($_POST) && $row == 0 -> !getValue('status',$row)
        //b) van post, de nincs benne a status !empty($_POST) && !getValue('status')
        if(
            !empty($_POST)
                &&
            !getValue('status')
            ||
            !getValue('status',$row)){
            $checked = '';
        }*/
        /*if( !(empty($_POST) && getValue('status',$row) || getValue('status')) ){
            $checked = '';
        }*/

        $form .= '<label>
        <span>
        <input type="checkbox" name="status" value="1" ' . $checked . '> Státusz
        </span>';
        $form .= '</label>
<button>Küldés</button>
</form>';

        $output = $form;//outputban legyen az űrlap most
        break;

    default:
        //lista
        $qry = "SELECT id,name,email,status FROM admins";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));

        $table = '<a href="?action=create">Új felvitel</a>
            <table border="1">
                    <tr>
                     <th>id</th>
                     <th>name</th>
                     <th>email</th>
                     <th>status</th>
                     <th>művelet</th>
                    </tr>';
        while ($row = mysqli_fetch_row($result)) {
            $table .= '<tr>
                     <td>' . $row[0] . '</td>
                     <td>' . $row[1] . '</td>
                     <td>' . $row[2] . '</td>
                     <td>' . $row[3] . '</td>
                     <td><a href="?action=edit&amp;id=' . $row[0] . '">módosít</a> | <a onclick="return confirm(\'Tuti biztos?\')" href="?action=delete&amp;id=' . $row[0] . '">töröl</a></td>
                    </tr>';
        }
        $table .= '</table>';
//kiírás
        $output = $table;//kiírandó ebben az ágban legyen a table
        break;
}

//kiírás 1 lépésben
echo $output;










/**
 * @todo HF:
 * Tervezd meg egy cikkek db tábla tervét
 * mezők, tipusok
 * Cím, bevezető, tartalom, státusz, publikálás ideje, kép(1), time_created,time_updated, szerző
 */
