<?php

require "database.php";/** @var $link mysqli db csatlakozás betöltése*/


$qry = "SELECT 
            off.country, 
            off.city, 
            SUM(priceeach*quantityordered) 'total_orders'
        FROM offices off
        LEFT JOIN employees e
            ON e.officecode = off.officecode
        LEFT JOIN customers c
            ON e.employeenumber = salesrepemployeenumber
        LEFT JOIN orders o
            ON o.customernumber = c.customernumber
        LEFT JOIN orderdetails od
            ON od.ordernumber = o.ordernumber
        GROUP BY off.officecode";

//lekérés futtatása
$result = mysqli_query($link,$qry) or die(mysqli_error($link));
//kibontás/bejárás - kiírás -> nem jó mert doctype előtt vagyunk ezért bodyban kell megtenni
//kibontás/bejárás - table->változóba gyüjtése while ciklussal
$table = '<table>
            <tr>
             <th>Ország</th>
             <th>Város</th>
             <th>Rendelések összesen</th>
            </tr>';
while( $row = mysqli_fetch_assoc($result) ) {
    //var_dump($row);
    $table .= '<tr>
                 <td>'.$row['country'].'</td>
                 <td>'.$row['city'].'</td>
                 <td>'.$row['total_orders'].'</td>
                </tr>';
}
$table .= '</table>';

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Adatbázis lekérések 5. feladat megvalósítása</title>
</head>
<body>
<h1>5. irodánként mennyit (összeg) rendeltek?</h1>
<?php
//kiírás
echo $table;
?>
</body>
</html>
