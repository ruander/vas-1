<?php
//adatbázis kapcsolat felépítése (procedurális) (XAMPP esetén)
$dbHost = 'localhost';
$dbUser = 'root';
$dbPass = '';
$dbName = 'classicmodels';
//db kapcsolat felépítése
$link = @mysqli_connect($dbHost,$dbUser,$dbPass,$dbName) or die('Hiba az adatbázis kapcsolatban! Keresse a rendszer üzemeltetőjét: ['.mysqli_connect_error().']');//return -> resource (object)
echo '<pre>';
//var_dump($link);

//lekérés összeállítása
$qry = "SELECT * FROM employees";
//lekérés a linken vagy ÁLLJ (hiba)
$result = mysqli_query($link, $qry) or die(mysqli_error($link)); //object
//var_dump($result);
/*
//egy sor kibontása az eredményből
$row = mysqli_fetch_row($result);
//var_dump($row);
//echo $row[4];

//egy sor kibontása az eredményből
$row = mysqli_fetch_assoc($result);
//var_dump($row);

//egy sor kibontása az eredményből
$row = mysqli_fetch_array($result);
//var_dump($row);

//egy sor kibontása az eredményből
$row = mysqli_fetch_object($result);
//var_dump($row);
//echo $row->email;//objektum orientált php prog. anyag
*/
//összes sor kibontása
$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);//$rows[0]['employeeNumber']
//var_dump($rows);//2 dimenziós tömb
/*
 * table nyitás
foreach($rows as $employee){

//adatsorok /employee
tr+td elemek, td elemek közé fűzve a megfelelő adat: pl $employee['firstName']

}
table zárás

//ha table változóba gyüjtöttük
table kiírása
*/


//employees táblázat elkészítése
$table = '<table border="1">';
foreach($rows as $employee){
    $table .= '<tr>
                <td>'.$employee['employeeNumber'].'</td>
                <td>'.$employee['firstName'].'</td>
                <td>'.$employee['lastName'].'</td>
                <td>'.$employee['email'].'</td>
                <td>'.$employee['extension'].'</td>
                <td>'.$employee['officeCode'].'</td>
                <td>'.$employee['reportsTo'].'</td>
                <td>'.$employee['jobTitle'].'</td>
                </tr>';

    //var_dump($employee);
}
$table .= '</table>';
//kiírás:
echo $table;
