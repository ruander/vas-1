<?php
include "../config/database.php";/** @var $link mysqli */
include "../config/functions.php";//eljárások
include "../config/settings.php";//beállítások
//munkafolyamatok
/**
 * Szuperglobális $_SESSION
 * eljárások: sessions php.net https://www.php.net/manual/en/book.session
 */
//mf indítás
session_start();
//$_SESSION['test']='Hello world!';
var_dump($_SESSION, session_id() );
$info = '';
if (!empty($_POST)) {
    if ( login() ) {
        //irány az index
        header('location:index.php');
        exit();
    } else {
        $info = '<span class="error">Nem megfelelő email/jelszó páros!</span>';
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin felület - belépés</title>
</head>
<body>
<form method="post">
    <h1>Adminisztráció - Belépés</h1>
    <div class="message">
        <?php echo $info; ?>
    </div>
    <label>
        <span>Email</span>
        <input type="text" name="email" value="<?php echo getValue('email'); ?>" placeholder="email@cim.hu">
    </label>
    <label>
        <span>Jelszó</span>
        <input type="password" name="password" value="" placeholder="******">
    </label>
    <button>Belépés</button>
</form>
</body>
</html>

