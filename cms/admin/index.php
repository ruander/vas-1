<?php
include "../config/database.php";/** @var $link mysqli */
include "../config/functions.php";//eljárások
include "../config/settings.php";//beállítások
//mf indítás
session_start();
//var_dump($_SESSION);
$p = filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT) ?: 0;//melyik menü aktív, vagy vezérlőpult
//alap URL összeállítása
$baseURL = "index.php?p=$p";
//kiléptetünk ha kell //mf roncsolás
if (filter_input(INPUT_GET, 'logout') !== null) {
    logout();
}
$auth = auth();
if (!$auth) {
    //irány a login
    header('location:login.php');
    exit();
}

//modul logika - $output változó létrehozása
$module = ADMIN_MENU[$p]['module'];//@todo ha belenyulnak az urlbe nem létező id val , notice javítása
$modulFile = MODULES_DIR . $module . MODULE_EXT;//settings
//ha létezik a modul töltsük be
if (file_exists($modulFile)) {
    include $modulFile;
    /** @var $output string */
} else {
    $output = "Nincs ilyen modul: $modulFile";
}

//userbar
$userbar = '<div class="userbar">Üdvözlet <b>' . $_SESSION['userdata']['username'] . '</b> | <a href="?logout">kilépés</a></div>';

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adminisztrációs felület</title>
    <!--stílusok-->
    <link rel="stylesheet" href="css/custom.css">
</head>
<body>
<?php echo $userbar; ?>
<header>
    <?php
    //Admin menü:
    echo makeAdminMenu();
    ?>
</header>
<main>
    <?php
    //modul kiírások (modulból)
    echo $output;
    ?>
</main>
<footer>lábléc</footer>
<!--scriptek-->
<script src="js/custom.js"></script>
</body>
</html>

