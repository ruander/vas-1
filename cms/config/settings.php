<?php
//általános beállítások
const SECRET_KEY = 'wauedgcziasc134';//titkosító kulcs
//admin modulok mappája és kiterjesztése
const MODULES_DIR = 'modules/';
const MODULE_EXT = '.php';
//menüpontok tárolója
const ADMIN_MENU = [
    0 => [
        'title' => 'Vezérlőpult',
        'icon' => 'fab fa-dashboard',
        'module' => 'dashboard'
    ],
    1 => [
        'title' => 'Adminisztrátorok',
        'icon' => 'fab fa-user',
        'module' => 'admins'
    ],
];
