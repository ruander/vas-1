<?php

/**
 * Admin menü készítése (HTML render)
 * @return string
 */
function makeAdminMenu()
{
    $adminMenu = '<nav><ul class="main-menu">';
    //menüpontok
    foreach (ADMIN_MENU as $menuID => $menuItem) {

        $adminMenu .= '<li>
                <a class="' . $menuItem['icon'] . '" href="?p=' . $menuID . '">' . $menuItem['title'] . '</a>
                </li>';

    }

    //menü zárása
    $adminMenu .= '</ul></nav>';

    return $adminMenu;
}

/**
 * login logika
 * @return bool
 */
function login()
{
    global $link;//lássuk a db csatlakozást
    $email = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'email'));
    $password = filter_input(INPUT_POST, 'password');
//lekérjük az emailhez tartozó jelszót
    $qry = "SELECT password,id,name FROM admins WHERE email = '$email' and status = 1 LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
//ha van row (azaz talált email egyezést) akkor password check
    if ($row !== null && password_verify($password, $row[0])) {
        //echo 'SIKER!';
        //ekkor történt a belépés:
        $stime = time();//UNIX timestamp
        $sid = session_id();//mf azonosító
        //MF jelszó md5(sid+userId+SECRET)
        $spass = md5($sid . $row[1] . SECRET_KEY);
        $_SESSION['userdata'] = [
            'id' => $row[1],
            'username' => $row[2],
            'email' => $email
        ];
        $_SESSION['id'] = $sid;
        //takarítsunk, ne legyen 'beragadt' azonosító a belépéskor
        mysqli_query($link, "DELETE FROM sessions WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        //tároljuk el a belépést az adatbázisban
        $qry = "INSERT INTO sessions (`sid`,`spass`,`stime`) VALUES('$sid','$spass',$stime)";
        mysqli_query($link, $qry) or die(mysqli_error($link));
        return true;
    }
    return false;
}

/**
 * Érvényes belépés ellenőrzése (azonosítás)
 * @return bool
 */
function auth(){
    global $link;//lássuk az eljárásban
    //$_SESSION,sessions táblában record,settings SECRET_KEY
    $now = time();
//öntisztítás
    $expired = $now - (60*15);//lejárt mf ok törlése (most 15 perc)
//töröljük a lejárt mf-okat
    mysqli_query($link,"DELETE FROM sessions WHERE stime < $expired") or die(mysqli_error($link));
//lekérjük a sessionhöz tartozó recordot
    $sid = session_id();
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);//ha volt ilyen rekord akkor a spass a $row[0]-n található
    //var_dump($row);
//spass most alapján tesztelni tudunk:
//ha nincs [userdata][id] a sessionben akkor eleve hiba, ha van de nem egyeznek a spass értékek hiba
    if(
        isset($_SESSION['userdata']['id'])
        &&
        $row[0] ===  md5($_SESSION['id'] . $_SESSION['userdata']['id'] . SECRET_KEY)
    ) {
        //minden oké, FRISSITJUK AZ stime-OT
        mysqli_query($link, "UPDATE sessions SET stime = $now WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        return true;
    }
    //gond volt....
    return false;
}

/**
 * Logout
 * :void
 */
function logout(){
    //roncsolunk amit érünk :)
    global $link;
    mysqli_query($link, "DELETE FROM sessions WHERE sid = '{$_SESSION['id']}' LIMIT 1") or die(mysqli_error($link));
    $_SESSION = [];
    session_destroy();
}

/**
 * value visszaadása input mezőkbe (paraméter átadással)
 * @param $fieldName
 * @param $rowData
 * @return mixed
 */
function getValue($fieldName, $rowData = []){
    if(filter_input(INPUT_POST, $fieldName) !== null){//ha létezik az elem akkor visszatér vele (akkor is ha üres)
        return filter_input(INPUT_POST, $fieldName);
    }
    //ha van DB adat akkor azzal térünk vissza
    if(array_key_exists($fieldName,$rowData)){
        return $rowData[$fieldName];
    }

    return '';
}

/**
 * hiba kiírás segédfüggvénye
 * @param $fieldName
 * @return string
 */
function getError($fieldName){
    global $errors;//az eljárás idejére globális, azaz 'látni' fogjuk az eljáráson belül

    if(isset($errors[$fieldName])){
        return $errors[$fieldName];//visszatérünk a hibaüzenettel
    }
    return '';//nincs ilyen elem visszatérünk üres stringgel
}
