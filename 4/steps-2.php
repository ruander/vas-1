<?php
//erőforrások
$step = 1;//itt járunk
//több lécsős ürlap
/*11.
Készítsünk programot, amely beolvas egy N természetes számot, majd billentyűzetről bekér N darab. természetes számot és ezeket a számokat összeadja, majd kiírja az eredményt.*/
echo '<pre>'.var_export($_POST,true).'</pre>';
if(!empty($_POST)){
    $errors = [];//üres hiba tömb
    //hibakezelés
    //n csak 2 és 10 közötti egész lehet
    $options = [
      'options' => [
          'min_range' => 2,
          'max_range' => 10
      ]
    ];

    $n = filter_input(INPUT_POST,'n',FILTER_VALIDATE_INT,$options);
    //var_dump($n);
    if( !$n ){
        $errors['n'] = '<span class="error">KETTŐ és TÍZ közötti EGÉSZ szám... (melyik nem érthető?)</span>';
    }else{
        //jó az n
        $step = 2;

    }


    $numbers = filter_input(INPUT_POST, 'numbers',FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY);//a numbers elemen egy tömböt kell találjunk
    echo '<pre>'.var_export($numbers,true).'</pre>';
    //hibakezelés ciklussal a számokra, ha kell
    if(is_array($numbers)){//ha tömböt kapunk a szűrés után
        foreach($numbers as $nr => $value){
            if($value === false){
                $errors['numbers'][$nr] = '<span class="error">hibás adat!</span>';
            }
        }
    }

    var_dump($errors);
    if(
        empty($errors) //nincs hibánk sem n sem numbers elemekben
        && //ÉS
        is_array($numbers)//ha ez az elem tömb akkor biztos hogy kaptunk számokat
    ){
        //kiírjuk a fent kiszámolt összeget; Mivel nincs hibánk, jó lesz az összeg...
        echo 'A számok összege:'.array_sum($numbers).'<a href="'.$_SERVER['PHP_SELF'].'">Újra</a>';
        die();
    }
}
$form = '<form method="post">';//form nyitás

//step alapján teszünk ki input mezőt a feladat szerint annyit amennyi az N
switch($step){
    case 2:
        $form .= "bekérjük a(z) $n darab számot";
        $oldNumbers = filter_input(INPUT_POST,'numbers',FILTER_DEFAULT,FILTER_REQUIRE_ARRAY);//segédtömb az összes értékkel a value-k kitöltéséhez
    echo '<pre>'.var_export($oldNumbers,true).'</pre>';
        //ciklus az input mezőknek
        for($i=1;$i<=$n;$i++){
            $form .='<label>
            <span>Szám '.$i.':</span>
            <input type="text" name="numbers['.$i.']" value="';
            //befűzzük a value-t, ha van (csak akkor nincs ilyen elem ha első betöltése van az űrlapnak
            if(isset( $oldNumbers[$i] )){
                $form .= $oldNumbers[$i];
            }
            $form .= '" placeholder="2-10">';
            //hibakírás, ha van
            if(isset( $errors['numbers'][$i] )){
                $form .= $errors['numbers'][$i];
            }
            $form .= '</label>';
        }
        //visszük az 'n'-t is tovább
    $form .= '<input type="hidden" name="n" value="'.$n.'">';
        break;
    default:
        $form .='<label>
            <span>Írd be hány számot akarsz összeadni:</span>
            <input type="text" name="n" value="'.getValue('n').'" placeholder="2-10">'.getError('n').'
         </label>';
        break;
}

$form .= '<input type="hidden" name="step" value="'.$step.'">';
$form .= '<button name="submit" value="'.$step.'">Mehet ('.$step.')</button>';
$form .= '</form>';//form zárása

//kiírás egy lépésben
echo $form;

/**
 * value visszaadása input mezőkbe (paraméter átadással)
 * @param $fieldName
 * @return mixed
 */
function getValue($fieldName){
    return filter_input(INPUT_POST, $fieldName);
}

//hiba 'kiíró' eljárás
//@todo, getError átalakítása VAGY új eljárás készítése a hibák kiírására azaz 2. dimenizóból [IS(!!!!)-ha átalakítod]
function getError($fieldName){
    global $errors;//az eljárás idejére globális, azaz 'látni' fogjuk az eljáráson belül

    if(isset($errors[$fieldName])){
        return $errors[$fieldName];//visszatérünk a hibaüzenettel
    }
    return '';//nincs ilyen elem visszatérünk üres stringgel
}

//ideiglenes stílusok
$styles = '<style>
        form, label  {
            display:flex;
            flex-flow: column nowrap;
        }
        form {
            max-width: 350px;
            margin:0 auto;
        }
        label {
            margin: 5px 0;
        }
        .error {
            color:#f00;
            font-style:italic;
            font-size:0.8em;
        }
    </style>';

echo $styles;
