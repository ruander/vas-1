<?php
//erőforrások
$step = 1;//itt járunk
//több lécsős ürlap
/*11.
Készítsünk programot, amely beolvas egy N természetes számot, majd billentyűzetről bekér N darab. természetes számot és ezeket a számokat összeadja, majd kiírja az eredményt.*/
//echo '<pre>'.var_export($_SERVER,true).'</pre>';
if(!empty($_POST)){
    $errors = [];//üres hiba tömb
    //hibakezelés
    //n csak 2 és 10 közötti egész lehet
    $options = [
      'options' => [
          'min_range' => 2,
          'max_range' => 10
      ]
    ];

    $n = filter_input(INPUT_POST,'n',FILTER_VALIDATE_INT,$options);
    //var_dump($n);
    if( !$n ){
        $errors['n'] = '<span class="error">KETTŐ és TÍZ közötti EGÉSZ szám... (melyik nem érthető?)</span>';
    }else{
        //jó az n
        $step = 2;

    }

    //hibakezelés ciklussal a számokra, ha kell
        //$test?
    //ha van a postban legalább number1 mező akkor beindítjuk a hibakezelésüket
    if(filter_input(INPUT_POST,'number1') !== null){
        $sum = 0;//ez lesz az összeg
        for($i=1;$i<=$n;$i++){
            $numberNow = filter_input(INPUT_POST, 'number'.$i,FILTER_VALIDATE_INT);
            if($numberNow === false){
                echo "<br>hiba:$i";
                $errors['number'.$i]='<span class="error">hibás adat!</span>';//beletesszük az aktuális mező hibáját a hibatömb megfelelő helyére (number1,number2 ....)
            }
            //gyűjtjük az összeget
            $sum += $numberNow;
        }
    }
    var_dump($errors);
    if(
        empty($errors)
        &&
        filter_input(INPUT_POST,'number1') !== null
        /*csak ha minden oké tényleg (numberek is, akkor menjünk be az igaz ágba*/
    ){
        //kiírjuk a fent kiszámolt összeget; Mivel nincs hibánk, jó lesz az összeg...
        echo 'A számok összege:'.$sum.'<a href="'.$_SERVER['PHP_SELF'].'">Újra</a>';
        die();
    }
}
$form = '<form method="post">';//form nyitás

//step alapján teszünk ki input mezőt a feladat szerint annyit amennyi az N
switch($step){
    case 2:
        $form .= "bekérjük a(z) $n darab számot";
        //ciklus az input mezőknek
        for($i=1;$i<=$n;$i++){
            $form .='<label>
            <span>Szám '.$i.':</span>
            <input type="text" name="number'.$i.'" value="'.getValue('number'.$i).'" placeholder="2-10">'.getError('number'.$i).'
         </label>';// 'number'.$i ==> "number$i"
        }
        //visszük az 'n'-t is tovább
    $form .= '<input type="hidden" name="n" value="'.$n.'">';
        break;
    default:
        $form .='<label>
            <span>Írd be hány számot akarsz összeadni:</span>
            <input type="text" name="n" value="'.getValue('n').'" placeholder="2-10">'.getError('n').'
         </label>';
        break;
}

$form .= '<input type="hidden" name="step" value="'.$step.'">';
$form .= '<button name="submit" value="'.$step.'">Mehet ('.$step.')</button>';
$form .= '</form>';//form zárása

//kiírás egy lépésben
echo $form;

/**
 * value visszaadása input mezőkbe (paraméter átadással)
 * @param $fieldName
 * @return mixed
 */
function getValue($fieldName){
    return filter_input(INPUT_POST, $fieldName);
}

//hiba 'kiíró' eljárás
function getError($fieldName){
    global $errors;//az eljárás idejére globális, azaz 'látni' fogjuk az eljáráson belül

    if(isset($errors[$fieldName])){
        return $errors[$fieldName];//visszatérünk a hibaüzenettel
    }
    return '';//nincs ilyen elem visszatérünk üres stringgel
}

//ideiglenes stílusok
$styles = '<style>
        form, label  {
            display:flex;
            flex-flow: column nowrap;
        }
        form {
            max-width: 350px;
            margin:0 auto;
        }
        label {
            margin: 5px 0;
        }
        .error {
            color:#f00;
            font-style:italic;
            font-size:0.8em;
        }
    </style>';

echo $styles;
