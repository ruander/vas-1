<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlap feldolgozása</title>
</head>
<body>
<section>
    <h1>Töltse ki az űrlapot (GET)</h1>
    <form action="feldolgoz.php" method="get">
        <label>
            <span>Név:</span>
            <input type="text" name="name" placeholder="Gipsz Jakab" value="">
        </label>
        <!--<input type="submit" name="submit" value="Gyííí!">-->
        <button>Mehet</button>
    </form>
</section>
<section>
    <h1>Töltse ki az űrlapot (POST)</h1>
    <form action="feldolgoz.php?name=teszt" method="post">
        <label>
            <span>Név:</span>
            <input type="text" name="name" placeholder="Gipsz Jakab" value="">
        </label>
        <button>Mehet</button>
    </form>
</section>
</body>
</html>
