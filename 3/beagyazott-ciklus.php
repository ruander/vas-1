<?php
//csináljunk egy táblázatot aminek a celláiba beleírjuk hogy hanyadik sor, hanyadik cellája

echo '<table border="1">';

for($i=1;$i<=6;$i++){
   echo "<tr>";
    for($j=1;$j<=12;$j++){
        echo "<td>$i/$j</td>";
    }
   echo "</tr>";
}

echo '</table>';
///////////////////////
$col = 15;//oszlopok száma
$row = 8;//sorok száma

$table ='<table border="1">';//nyitás

for($i=1;$i<=$row;$i++){
    $table .= "<tr>";
    for($j=1;$j<=$col;$j++){
        $table .= "<td>$i/$j</td>";
    }
    $table .= "</tr>";
}

$table .='</table>';//zárás
//kiírás 1 lépésben
echo $table;

///////////////////////
$col = 15;//oszlopok száma
$row = 8;//sorok száma

//szinek
$r = rand(0,255);//piros összetevő
$g = rand(0,255);//zöld összetevő
$b = rand(0,255);//kék összetevő
$table ='<table style="background-color:rgb('.$r.','.$g.','.$b.')">';//nyitás

for($i=1;$i<=$row;$i++){
    $table .= "<tr>";
    for($j=1;$j<=$col;$j++){
        $table .= "<td>$i/$j</td>";
    }
    $table .= "</tr>";
}

$table .='</table>';//zárás
//kiírás 1 lépésben
echo $table;

///////////////////////
$col = 15;//oszlopok száma
$row = 8;//sorok száma

$table ='<table style="border-collapse: collapse;">';//nyitás

for($i=1;$i<=$row;$i++){
    //szinek
    $r = rand(0,255);//piros összetevő
    $g = rand(0,255);//zöld összetevő
    $b = rand(0,255);//kék összetevő
    $table .= "<tr style=\"background-color: rgb($r,$g,$b);\">";
    for($j=1;$j<=$col;$j++){
        $table .= "<td>$i/$j</td>";
    }
    $table .= "</tr>";
}

$table .='</table>';//zárás
//kiírás 1 lépésben
echo $table;

///////////////////////
$col = rand(40,80);//oszlopok száma
$row = rand(15,25);//sorok száma

$table ='<table style="width:100%;border-collapse: collapse;">';//nyitás

for($i=1;$i<=$row;$i++){

    $table .= "<tr>";
    for($j=1;$j<=$col;$j++){
        //szinek
        $r = rand(0,255);//piros összetevő
        $g = rand(0,255);//zöld összetevő
        $b = rand(0,255);//kék összetevő
        $table .= "<td style=\"background-color: rgb($r,$g,$b);\">&nbsp;</td>";
    }
    $table .= "</tr>";
}

$table .='</table>';//zárás
//kiírás 1 lépésben
echo $table;
