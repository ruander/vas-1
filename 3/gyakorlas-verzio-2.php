<?php
//35. Készítsünk programot, amely beolvas egy egész számot (N), majd kiírja szavakkal, hogy a hét N-dik napja milyen nap (hétfő, kedd, szerda, ...). Például: A hét 5. napja -> péntek

//36. Ha olyan számot adunk meg, ami kisebb mint 1 vagy nagyobb mint 7, akkor: Kerek egy egész számot: 8
//A hétnek csak hét napja van, nincs 8. napja!
//erőforrások
$output = '';//ide gyűjtjük a kiírandó elemeket
$days = [
    1 => 'hétfő',
    2 => 'kedd',
    3 => 'szerda',
    4 => 'csütörtök',
    5 => 'péntek',
    6 => 'szombat',
    7 => 'vasárnap'
];//hét napjai
//ha van $_POST adat akkor 'hibakezelés'
if (!empty($_POST)) { //operátor -> ! - negálás 'nem'
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    $errors = [];//ide gyűjtjük a hibákat
    //number mező 1-7 ig
    $number = filter_input(INPUT_POST, 'number');
    //var_dump($number);
    //array_key_exists(key,array)
    if (array_key_exists($number,$days) === false ) {
        $errors['number'] = '<span class="error">1-7 ig kéne megadni egy számot!</span>';
    }

    //var_dump($errors);

    if (empty($errors)) {
        //üres maradt a hibatömb hibakezelés után
        //művelet
        $output .= "<h3>A hét $number. napja {$days[$number]}</h3>";
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlap feldolgozása - azonos fileban</title>
    <style>
        form, label {
            display: flex;
            flex-flow: column nowrap;
        }

        form {
            max-width: 350px;
            margin: 0 auto;
        }

        label {
            margin: 5px 0;
        }

        .error {
            color: #f00;
            font-style: italic;
            font-size: 0.8em;
        }
    </style>
</head>
<body>
<?php
echo $output;//fent a php tagben gyüjtjük össze a tartalmát a logika alatt
?>


<section>
    <h1>Töltse ki az űrlapot (POST)</h1>
    <form method="post">
        <label>
            <span>Adj meg számot 1-7 ig:</span>
            <input type="text" name="number" placeholder="5" value="<?php echo filter_input(INPUT_POST, 'number'); ?>">
            <?php
            //ha van hibája az elemnek, akkor kiírjuk
            if (isset($errors['number'])) {
                echo $errors['number'];
            }
            ?>
        </label>
        <button>Mehet</button>
    </form>
</section>
</body>
</html>
