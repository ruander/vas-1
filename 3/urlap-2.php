<?php
/**
 * @todo HF: feladgyujtemény pdf 12. ig
 */
//ha van $_POST adat akkor 'hibakezelés'
if (!empty($_POST)) { //operátor -> ! - negálás 'nem'
    echo '<pre>' . var_export($_POST, true) . '</pre>';
//echo 'hello '.$_POST['name'];//közvetlen kulcsal ne használd a $_POST és GET tömböket!!!
    //hibakezelés
    $errors = [];//ide gyűjtjük a hibákat
    //Név mező nem lehet üres
    $name = strip_tags(filter_input(INPUT_POST, 'name'));//szűrés és spacek eltávolítása
    //$name = trim($name);//ltrim rtrim - szövegvégi spacek eltávolítása
    if($name == ""){
        $errors['name'] = '<span class="error">Kötelező megadni!</span>';
    }

    //email legyen email formátum
    $email = filter_input(INPUT_POST,'email', FILTER_VALIDATE_EMAIL);
    var_dump($email);
    if(!$email){
        $errors['email'] = '<span class="error">Érvénytelen formátum!</span>';
    }

    if(empty($errors)){
        //üres maradt a hibatömb hibakezelés után
        //die('Hello '.$name);
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlap feldolgozása - azonos fileban</title>
    <style>
        form, label  {
            display:flex;
            flex-flow: column nowrap;
        }
        form {
            max-width: 350px;
            margin:0 auto;
        }
        label {
            margin: 5px 0;
        }
        .error {
            color:#f00;
            font-style:italic;
            font-size:0.8em;
        }
    </style>
</head>
<body>
<section>
    <h1>Töltse ki az űrlapot (POST)</h1>
    <form method="post">
        <!--Név-->
        <label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" placeholder="Gipsz Jakab" value="<?php echo filter_input(INPUT_POST, 'name'); ?>">
            <?php
            //ha van hibája az elemnek, akkor kiírjuk
            if(isset($errors['name'])){
                echo $errors['name'];
            }
            ?>
        </label>
        <!--Email-->
        <label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" placeholder="email@cim.hu" value="<?php echo filter_input(INPUT_POST, 'email'); ?>">
            <?php
            //ha van hibája az elemnek, akkor kiírjuk
            if(isset($errors['email'])){
                echo $errors['email'];
            }
            ?>
        </label>
        <button>Mehet</button>
    </form>
</section>
</body>
</html>
