<?php
/**
 * A szuperglobális $_GET tömb
 */
echo '<pre>'.var_export($_GET,true).'</pre>';
/**
 * A szuperglobális $_POST tömb
 */
echo '<pre>'.var_export($_POST,true).'</pre>';
/**
 * A szuperglobális $_REQUEST tömb a $_GET és $_POST összeolvasztása ha egyezés van névben, akkor a POST az erősebb
 */
echo '<pre>'.var_export($_REQUEST,true).'</pre>';
